## Interface: 80000
## Title: Tukui Scrap for |cfff80000Tukui|r
## Notes: Adds the junk icon on Tukui bagslot items which are considered junk by Scrap.
## Author: Kasharg
## Dependencies: Scrap, Tukui
## Version: 0.01

Tukui_Scrap.lua
