local TukuiBags = Tukui[2].Inventory.Bags

-- Update Icon on bag slot
local function SlotUpdate(id, button)
    local BagID = id
    local Slot = button:GetID()
    local ItemLink = GetContainerItemLink(BagID, Slot)
    local Texture, Count, Lock, Quality, _, _, _, _, _, ItemID = GetContainerItemInfo(BagID, Slot)

    if (button.ItemID == ItemID) then
        return
    end
    button.ItemID = ItemID

    local id
    if ItemLink then
        id = tonumber(strmatch(ItemLink, 'item:(%d+)'))
    end

    if Button.JunkIcon then
        if id and Scrap:IsJunk(id, BagId, Slot) then
            Button.JunkIcon:SetShown(Scrap_Icons)
        else
            Button.JunkIcon:Hide()
        end
    end
end
hooksecurefunc(TukuiBags, "SlotUpdate", SlotUpdate)

-- Function we can call to update all bag slots
local function UpdateBags()
	TukuiBags:UpdateAllBags()
end

-- Set Hooks
local function SetHooks()
	hooksecurefunc(Scrap, 'SettingsUpdated', UpdateBags)
	hooksecurefunc(Scrap, 'ToggleJunk', UpdateBags)
	
	UpdateBags()

	Scrap.HasSpotlight = true
end
hooksecurefunc(TukuiBags, "Enable", SetHooks)
